Feature: Launching Calculator Windows Application

  Background: Open in Standard Mode
    Given user launches the application
    When user clicks on button "Open Navigation"
    Then user clicks on button "Standard Calculator"
    And user clicks on button "Close Navigation"

#  @only
  Scenario: Check the Mode
    Given calculator app is launched and checking the mode

#  @only
  Scenario: Click on Random Buttons to Check the Functionality
    Given calculator app is launched and checking the mode
    When user clicks on button "One"
    Then user clicks on button "Two"
    Then user clicks on button "One"
    And user clicks on button "Two"
    Then user clicks on button "Backspace"
    Then user clicks on button "Clear"

#  @only
  Scenario: Launches the Application and Adds two numbers and checks the output
    Given calculator app is launched and checking the mode
    When user clicks on button "One"
    Then user clicks on button "Plus"
    And user clicks on button "Two"
    And user clicks on button "Equals"
    Then check the result to be "3"

#    @only
  Scenario: Switch Between Different Modes
    Given calculator app is launched and checking the mode
    When user clicks on button "Open Navigation"
    Then user clicks on button "Scientific Calculator"
    And user clicks on button "Close Navigation"
    Then check the element to be "Scientific" of "Scientific"
    And user clicks on button "Open Navigation"
    Then user clicks on button "Programmer Calculator"
    And user clicks on button "Close Navigation"
    Then check the element to be "Programmer" of "Programmer"

#  @only
  Scenario: Find OutPut of The Entered String
    Given calculator app is launched and checking the mode
    When user enters a "1+2=3" for calculation validate it

#  @only
  Scenario Outline: Simply Input a few Strings
    Given  calculator app is launched and checking the mode
    When user enters an input as "<input>"
    Examples:
      | input |
      | 21.2  |
      | 1+2   |