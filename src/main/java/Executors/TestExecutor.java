package Executors;

import io.appium.java_client.windows.WindowsDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class TestExecutor {

    private static final Logger LOGGER = Logger.getLogger(TestExecutor.class);
    public static WindowsDriver driver;
    TestExecutor testExecutor;


    public boolean initialiseDriver() {
        try {
            LOGGER.info("Initialising Driver...");
            DesiredCapabilities cap = new DesiredCapabilities();
            cap.setCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
            cap.setCapability("platformName", "Windows");
            cap.setCapability("deviceName", "WindowsPC");

            try {
                driver = new WindowsDriver(new URL("http://127.0.0.1:4723/"), cap);
                LOGGER.debug("Connected to Calculator on the URL http://127.0.0.1:4723/");
            } catch (Exception e) {
                System.out.println(e);
            }
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            return true;
        } catch (Exception e) {
            LOGGER.error("Could not start the Calculator Application/ Problem in initialising Driver\n" + e);
            return false;
        }
    }

    public boolean startTheApplication() {
        try {
            LOGGER.info("Starting the Application");
            testExecutor = new TestExecutor();
            testExecutor.initialiseDriver();
            return true;
        } catch (Exception e) {
            LOGGER.error("Could not start Calculator Application");
            return false;
        }
    }

    public boolean checkStandardMode() {
        LOGGER.debug("Checking Standard Mode of The Calculator");
        try {
            String mode = driver.findElementByAccessibilityId("Header").getText();
            LOGGER.debug("Mode is : " + mode);
            System.out.println("Mode is : " + mode);
            if (mode.equals("Standard")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Problem in checking/setting the Standard Mode\n" + e);
            LOGGER.error("Problem in checking/setting the Standard Mode\n" + e);
            return false;
        }
    }


    public void clickOnButton(String button) {
        try {
            System.out.println("Trying to press button: " + button);
            LOGGER.debug("Clicking on Button" + button);
            driver.findElementByName(button).click();
        } catch (Exception e) {
        }
    }

    public String getTextOfTheElement(String elementName) {
        LOGGER.debug("Getting Value of Element is:" + driver.findElementByClassName(elementName).getText());
        return driver.findElementByName(elementName).getText();
    }

    public boolean checkIfTheStringMatches(String actual, String expected) {
        LOGGER.debug("The Actual String is :" + actual + " and the Expected String is : " + expected);
        if (expected.equals(testExecutor.getTextOfTheElementByAutomationId(actual))) {
            return true;
        } else return false;
    }

    public String getTextOfTheElementByAutomationId(String elementName) {
        return driver.findElementByAccessibilityId("Header").getText();
    }

    public boolean checkIfTheResultMatches(String expected) {
        LOGGER.debug("The Expected String is : " + expected);
        String result = driver.findElementByAccessibilityId("CalculatorResults").getText();
        System.out.println("The Expected String is : " + expected + " and Got: " + result);
        LOGGER.debug("The Expected String is : " + expected + " and Got: " + result);
        if (("Display is " +expected).equals(result)) {
            return true;
        } else return false;
    }

    public void cleanUp() {
        driver.quit();
        LOGGER.info("Ending Test Case");
    }

    public boolean splitTheStringAndCalculate(String enteredString) {
        try {
            testExecutor= new TestExecutor();
            LOGGER.debug("Entered String is : " + enteredString);
            System.out.println("Entered String is: " + enteredString);

            String strArray[] = enteredString.split("=");
            String stringBeforeEquals = strArray[0];

            for (String a : strArray) {
                System.out.println(a);
            }

            String characterAtTheString;

            for (int i = 0; i < stringBeforeEquals.length(); i++) {
                characterAtTheString = Character.toString(stringBeforeEquals.charAt(i));
                LOGGER.debug("Character at: " + characterAtTheString);
                System.out.println("Character at: " + characterAtTheString);
                LOGGER.debug("Value of the String from Map: " + keyMap(characterAtTheString));
                System.out.println("Value of the String from Map: " + keyMap(characterAtTheString));
                driver.findElementByName(
                        keyMap(characterAtTheString)
                ).click();
            }

            String calculatedValueExpected = strArray[1];

            testExecutor.clickOnButton("Equals");

            if(testExecutor.checkIfTheResultMatches(calculatedValueExpected)){
                System.out.println("Checking if the result matches for calculated Value: " + calculatedValueExpected);
                LOGGER.debug("Checking if the result matches for calculated Value: " + calculatedValueExpected);
                return true;
            }
            return false;
        } catch (Exception e) {
            LOGGER.error("splitTheStringAndCalculate failed to compute");
            return false;
        }
    }

    public void enterAnInput(String inputString){
        try {
            System.out.println("Entered String is: " + inputString);

            String input;

            for (int i = 0; i < inputString.length(); i++) {
                input = Character.toString(inputString.charAt(i));
                LOGGER.debug("Character at: " + input);
                System.out.println("Character at: " + input);
                LOGGER.debug("Value of the String from Map: " + keyMap(input));
                System.out.println("Value of the String from Map: " + keyMap(input));
                driver.findElementByName(
                        keyMap(input)
                ).click();
            }
            Thread.sleep(5000);
        }catch(Exception e){

        }

    }

    protected String keyMap(String key){
// we can keep this in a properties file as well instead of hard coding here
        Map map = new HashMap<String, String>();
        map.put("1", "One");
        map.put("2", "Two");
        map.put("3", "Three");
        map.put("4", "Four");
        map.put("5", "Five");
        map.put("6", "Six");
        map.put("7", "Seven");
        map.put("8", "Eight");
        map.put("9", "Nine");
        map.put(".", "Decimal Separator");
        map.put("+", "Plus");
        map.put("-", "Minus");

        return (String) map.get(key);


    }

}
