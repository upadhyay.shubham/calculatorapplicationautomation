package Executors;

import cucumber.api.Scenario;

import java.io.File;
import java.io.PrintStream;

public class Utilties {

    public void log(){
        try {
            File file = new File("log//logging.txt");
            PrintStream stream = new PrintStream(file);
            System.out.println("From now on " + file.getAbsolutePath() + " will be your console");
            System.setOut(stream);
        }catch (Exception e){

        }
    }
}
