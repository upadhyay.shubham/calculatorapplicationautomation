import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src\\main\\resources\\Feature\\Calculator.feature"},
        glue = {"stepDefinitions"},
        plugin = {"pretty", "html:target\\cucumber"},
//        tags = {"@only"},
        monochrome = true)

public class TestRunner {


}