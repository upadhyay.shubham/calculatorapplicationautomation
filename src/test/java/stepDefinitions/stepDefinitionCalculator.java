package stepDefinitions;

import Executors.TestExecutor;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;

public class stepDefinitionCalculator {

    public TestExecutor testExecutor = new TestExecutor();

    @Given("^user launches the application$")
    public void user_launches_the_application() {
        assertTrue(testExecutor.startTheApplication());
    }

    @Given("^calculator app is launched and checking the mode$")
    public void calculator_app_is_launched_and_checking_the_mode() {
       testExecutor.checkStandardMode();
    }

    @When("^user clicks on button \"([^\"]*)\"$")
    public void user_clicks_on_button(String button) {
       testExecutor.clickOnButton(button);

    }

    @Then("^check the result to be \"([^\"]*)\"$")
    public void check_the_result_to_be(String expected) {
        assertTrue(testExecutor.checkIfTheResultMatches(expected));
    }

    @Then("^check the element to be \"([^\"]*)\" of \"([^\"]*)\"$")
    public void check_the_element_to_be_of(String actual, String expected) {
        assertTrue(testExecutor.checkIfTheStringMatches(actual, expected));
    }

    @When("^user enters a \"([^\"]*)\" for calculation validate it$")
    public void user_enters_a_for_calculation(String string) {
        assertTrue(testExecutor.splitTheStringAndCalculate(string));
    }

    @When("^user enters an input as \"([^\"]*)\"$")
    public void user_enters_an_input_as(String inputString) {
        testExecutor.enterAnInput(inputString);
    }

    @After
    public void tearDown() {
        testExecutor.cleanUp();
    }

}
