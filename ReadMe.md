1. This project uses BDD as framework. Cucumber.
2. The feature File is placed in Resources Folder
3. You can reuse test steps provided in the feature file.
4. Each run creates a Log folder with current timestamp in EPOCH. The log file is placed inside it.
5. src/main/java/Executors contains the core logic implemented.

How to run?

1. Start your appium server. 
2. Set the appium server url in line- 34 of TestExecutor class.
3. Right Click on src/test/java/TestRunner.java and click run.